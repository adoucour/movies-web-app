import axios from "axios";
import React, { useEffect } from "react";
import { useState } from "react/cjs/react.development";
import Card from "../components/Card";
import Header from "../components/Header";

const UserList = () => {
  const [listData, setListData] = useState([]);

  useEffect(() => {
    let moviesId = window.localStorage.moviesList
      ? window.localStorage.moviesList.split(",")
      : [];

    for (let element of moviesId) {
      axios
        .get(
          `https://api.themoviedb.org/3/movie/${element}?api_key=6998b4df49c52ae8637a186702440661&language=fr-FR`
        )
        .then((result) =>
          setListData((listData) => [...listData, result.data])
        );
    }
  }, []);

  return (
    <div className="user-list-page">
      <Header />
      <h2>
        Favoris <span>❤️</span>
      </h2>
      <div className="result">
        {listData.length > 0 ? (
          listData.map((movie) => <Card movie={movie} key={movie.id} />)
        ) : (
          <h2>Aucun Favori</h2>
        )}
      </div>
    </div>
  );
};

export default UserList;
