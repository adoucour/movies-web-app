import axios from "axios";
import React, { useEffect, useState } from "react";
import Card from "./Card";

const Form = () => {
  const [moviesData, setMoviesData] = useState([]);
  const [search, setSearch] = useState("code");
  const callGetTmdb = `https://api.themoviedb.org/3/search/movie?api_key=6998b4df49c52ae8637a186702440661&query=${search}&language=fr-FR`;
  const [sortGoodBad, setSortGoddBad] = useState(null);

  // when the component is mounted this code will be executed, [] is callback
  useEffect(() => {
    axios
      .get(callGetTmdb)
      .then((response) => setMoviesData(response.data.results));
  }, [callGetTmdb]);

  return (
    <div className="form-component">
      <div className="form-container">
        <form>
          <input
            type="text"
            placeholder="Entrez le titre du film"
            id="search-input"
            onChange={(event) => setSearch(event.target.value)}
          />
          {/* <input type="submit" value="Rechercher" /> */}
        </form>
        <div className="btn-sort-container">
          <div
            className="btn-sort"
            id="goodToBad"
            onClick={() => setSortGoddBad("goodToBad")}
          >
            Top<span>➜</span>
          </div>
          <div
            className="btn-sort"
            id="badToGood"
            onClick={() => setSortGoddBad("badToGood")}
          >
            Flop<span>➜</span>
          </div>
        </div>
      </div>
      <div className="result">
        {moviesData
          .slice(0, 12)
          // eslint-disable-next-line array-callback-return
          .sort((a, b) => {
            if (sortGoodBad === "goodToBad") {
              return b.vote_average - a.vote_average;
            } else if (sortGoodBad === "badToGood") {
              return a.vote_average - b.vote_average;
            }
          })
          .map((movie) => (
            <Card key={movie.id} movie={movie} />
          ))}
      </div>
    </div>
  );
};

export default Form;
