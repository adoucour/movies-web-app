import { useReducer } from "react";
import crushReducer from "../reducers/crush.reducer";

const Card = ({ movie }) => {
  // const getTmdbGenre =
  //   "https://api.themoviedb.org/3/genre/movie/list?api_key=6998b4df49c52ae8637a186702440661&language=fr-FR";
  const getImgTmdb = "https://image.tmdb.org/t/p/original/";

  const [crushList, dispatch] = useReducer(crushReducer, {}, init);

  console.log(crushList);

  function init() {
    return { crush: [window.localStorage.moviesList] };
  }

  /**
   * @function @private
   * @name dateFormatter - converts api date format to french format (DD/MM/YYY)
   * @param {String} date - api date fomat (YYYY-MM-DD)
   * @returns {String} french date
   */
  const dateFormatter = (date) => {
    let [yy, mm, dd] = date.split("-");
    return [dd, mm, yy].join("/");
  };

  /**
   * @function @private
   * @name genreFinder - returns the genre of a movie by the genre id from the api
   * @returns {Array} containing the genre of each movie
   */
  const genreFinder = () => {
    let genreArray = [];

    for (let element of movie.genre_ids) {
      switch (element) {
        case 28:
          genreArray.push(`Action`);
          break;
        case 12:
          genreArray.push(`Aventure`);
          break;
        case 16:
          genreArray.push(`Animation`);
          break;
        case 35:
          genreArray.push(`Comédie`);
          break;
        case 80:
          genreArray.push(`Policier`);
          break;
        case 99:
          genreArray.push(`Documentaire`);
          break;
        case 18:
          genreArray.push(`Drame`);
          break;
        case 10751:
          genreArray.push(`Famille`);
          break;
        case 14:
          genreArray.push(`Fantasy`);
          break;
        case 36:
          genreArray.push(`Histoire`);
          break;
        case 27:
          genreArray.push(`Horreur`);
          break;
        case 10402:
          genreArray.push(`Musique`);
          break;
        case 9648:
          genreArray.push(`Mystère`);
          break;
        case 10749:
          genreArray.push(`Romance`);
          break;
        case 878:
          genreArray.push(`Science-fiction`);
          break;
        case 10770:
          genreArray.push(`Téléfilm`);
          break;
        case 53:
          genreArray.push(`Thriller`);
          break;
        case 10752:
          genreArray.push(`Guerre`);
          break;
        case 37:
          genreArray.push(`Western`);
          break;
        default:
          break;
      }
    }
    return genreArray.map((genre) => <li key={genre}>{genre}</li>);
  };

  /**
   * @function @private {void}
   * @name addStorage - add a movie from the favorites list in the localStorage,
   */
  const addStorage = () => {
    let storedData = window.localStorage.moviesList
      ? window.localStorage.moviesList.split(",")
      : [];

    // if it is already in the list it will not be added
    if (!storedData.includes(movie.id.toString())) {
      storedData.push(movie.id);
      window.localStorage.moviesList = storedData;
    }

    dispatch({ type: "crush" });
  };

  /**
   * @function @private {void}
   * @name deleteStorage - remove a movie from the favorites list in the localStorage
   */
  const deleteStorage = () => {
    let storedData = window.localStorage.moviesList.split(",");
    let newStoredData = storedData.filter((id) => parseInt(id) !== movie.id);
    window.localStorage.moviesList = newStoredData;
  };

  /**
   * @function @private
   * @name disabledAddButton - disable the add to favorites button if the movie id is in the favorites list
   * @param {Number} id - id movie
   * @returns {Boolean}
   * true disable button
   */
  const disabledAddButton = (id) => {
    if (window.localStorage.moviesList !== undefined) {
      let storedMovies = window.localStorage.moviesList.split(",");
      return storedMovies.includes(id.toString());
    } else {
      return false;
    }
  };

  return (
    <div className="card">
      <img
        src={
          movie.backdrop_path
            ? getImgTmdb + movie.backdrop_path
            : "./img/bobine-de-film.jpg"
        }
        alt="affiche film"
      />
      <h2>{movie.title}</h2>
      {movie.release_date ? (
        <h5>Date de sortie : {dateFormatter(movie.release_date)}</h5>
      ) : (
        ""
      )}
      <h4>
        {movie.vote_average}/10 <span>⭐</span>
      </h4>
      <ul>
        {movie.genre_ids
          ? genreFinder()
          : movie.genres.map((genre, index) => (
              <li key={index}>{genre.name}</li>
            ))}
      </ul>
      {movie.overview ? <h3>Synopsis</h3> : ""}
      <p>{movie.overview}</p>

      {movie.genre_ids ? (
        <div
          className="btn"
          onClick={() => addStorage()}
          style={
            disabledAddButton(movie.id)
              ? { pointerEvents: "none", opacity: "0.4" }
              : {}
          }
        >
          Favoris
        </div>
      ) : (
        <div
          className="btn"
          onClick={() => {
            deleteStorage();
          }}
        >
          Supprimer favori
        </div>
      )}
    </div>
  );
};

export default Card;
